
/*Задание№1*/

/*var myForm = Array.from(document.forms[0].elements);
var jsonObj = {};

var submitBtn = document.getElementById('submitBtn');
submitBtn.addEventListener('click',function (event) {
    event.preventDefault();
    myForm.forEach( function(item){
        if( item.type !== "submit"){
            jsonObj[item.name] = item.value;
        }
    });
    var result = JSON.stringify(jsonObj)
    console.log(result);
    console.log(JSON.parse(result));

});
*/

/*Задание№2
Написать фильтр массива по:
Актеру, продолжительности
*/

var stars = ['Elijah Wood', 'Ian McKellen', 'Orlando Bloom','Liam Neeson', 'Natalie Portman', 'Ewan McGregor', 'Billy Bob Thornton', 'Martin Freeman']
var x = [
    {
        name: "Lord of the Rigs",
        duration: 240,
        starring: [ 'Elijah Wood', 'Ian McKellen', 'Orlando Bloom']
    },
    {
        name: "Star Wars: Episode I - The Phantom Menace",
        duration: 136,
        starring: [ 'Ewan McGregor',' Liam Neeson', 'Natalie Portman']
    },
    {
        name: "Fargo",
        duration: 100,
          starring: [ 'Ewan McGregor', 'Billy Bob Thornton', 'Martin Freeman']
    },
    {
        name: "V for Vendetta",
        duration: 150,
        starring: [ 'Hugo Weaving', 'Natalie Portman', 'Rupert Graves']
    },
    {
        name: "Star Wars: The Force Awakens",
        duration: 130,
        starring: [ 'Ewan McGregor', 'Billy Bob Thornton', 'Martin Freeman' ]
    }
];

var actors = document.getElementById('actors');
var movieDuration = document.getElementById('movieDuration');
var findMovie = document.getElementById('findMovie');

stars.forEach(function (element) {
    var option = document.createElement('option');
    option.innerHTML = element;
    option.setAttribute('id',element);
    actors.appendChild(option);
});

findMovie.addEventListener('submit', function (event) {

    event.preventDefault();

    var resultDiv = document.createElement('div');

    x.forEach(function (elem) {
        var movieUL = document.createElement('ul');
        if(elem.starring.indexOf(actors.value) > -1 && elem.duration >= movieDuration.value){
            console.log('works');
            for(var key in elem){
                console.log(key);
                var movieLI = document.createElement('li');
                movieLI.innerHTML = key + ': ' + elem[key];
                movieUL.appendChild(movieLI);
            }
        }
        console.log(movieUL);
        if(movieUL.children.length > 0){
            resultDiv.appendChild(movieUL);
            document.body.appendChild(resultDiv);
        }

    });

    resultDiv = document.createElement('div');
    document.body.appendChild(resultDiv);

});

